from django.shortcuts import render
    
def index(request):
    response = { 'name' : 'Alsabila Shakina Prasetyo'}
    myProfile = 'I am a third year Computer Science student in University of Indonesia. I am interested in Operating System, '
    myProfile += 'Front End/Back End/Full Stack programming, and IT Support. However, working in other fields is not a problem for me. '
    myProfile += ' Other than CS/IT, I also have advanced skills in playing classical piano. '
    myProfile += 'I am confident in my communication skills, cooperativeness, creativity, and I take full responsibility on my works. I am eager to teach and be taught by others.'
    response['profile'] = myProfile

    response['computer'] = 'Python, Java, C++, HTML, CSS, JavaScript, SQL, Git, Django and Spring Framework.'
    response['language'] = 'Bahasa Indonesia, English.'
    response['personal'] = 'Good Communication Skills, Ability to Work Under Pressure, Cooperativeness and Creativity'

    return render(request, 'dashboard.html', response)
